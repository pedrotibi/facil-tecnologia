TESTE DE TI - LÓGICA

Pergunta 1 de 2

Em um sistema de cadastro de contratos, você tem as seguintes tabelas (Tb) e campos:


========================

Tb_contrato

    codigo

    prazo

    valor

    data_inclusao

    convenio_servico

------------------------------------

Tb_convenio_servico

    codigo

    convenio

    servico

------------------------------------

Tb_convenio

    codigo

    convenio

    verba

    banco

------------------------------------

Tb_banco

    codigo

    nome

========================

 

Considerando que quando um campo é utilizado para ligar uma tabela a outra, este campo terá como nome, o nome da segunda tabela.

 

Construa um script PHP que execute a consulta e liste uma relação contendo: 

========================   

nome do banco

    verba

    codigo do contrato

    data de inclusao

    valor

    prazo

========================




Em um sistema de cadastro de contratos, você tem as seguintes tabelas (Tb) e campos:


========================

Tb_contrato

    codigo

    prazo

    valor

    data_inclusao

    convenio_servico

------------------------------------

Tb_convenio_servico

    codigo

    convenio

    servico

------------------------------------

Tb_convenio

    codigo

    convenio

    verba

    banco

------------------------------------

Tb_banco

    codigo

    nome

========================

 

Considerando que quando um campo é utilizado para ligar uma tabela a outra, este campo terá como nome, o nome da segunda tabela.

 

Construa uma consulta SQL que liste uma relação contendo: 

========================   

    nome de cada banco (g)

    verba (g)

    data de inclusao do contrato mais antigo deste agrupamento

    data de inclusao do contrato mais novo deste agrupamento

    soma do valor dos contratos neste agrupamento

 

(g) = campos agrupados

========================
