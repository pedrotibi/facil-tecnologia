SELECT
	bc.nome AS nome_do_banco,
	cv.verba,
	MIN(ct.data_inclusao) AS inclusao_mais_antiga,
	MAX(ct.data_inclusao) AS inclusao_mais_recente,
	SUM(ct.valor) AS total
FROM
	tb_contrato ct
	INNER JOIN tb_convenio_servico cs ON ct.convenio_servico = cs.codigo
	INNER JOIN tb_convenio cv ON cs.convenio = cv.codigo
	INNER JOIN tb_banco bc ON cv.banco = bc.codigo
GROUP BY
	nome_do_banco, verba;