<?php

 ///  DEFINIÇÕES DE CONSTANTES >>>
 define('DB_SERVER', 'localhost');
 define('DB_PORT', '3306');
 define('DB_SCHEMA', 'banco_de_dados_mysql');
 define('DB_USER', 'user');
 define('DB_PASSWORD', 'P@ssword123');
 define('DB_CHARSET', 'utf8');
 ///  <<< DEFINIÇÕES DE CONSTANTES

 ///  CONEXÃO COM O BANCO DE DADOS >>>
 try{
    $connection = new mysqli(DB_SERVER . ':' . DB_PORT, DB_USER, DB_PASSWORD);
 }catch(Exception $e){
    echo('[ERROR 01] - ERRO AO ESTABELECER CONEXÃO COM O BANCO DE DADOS (' . $e->getMessage() . ')');
    exit();
 }

 try{
    if($connection->connect_errno){
        echo('[ERROR 02] - ERRO AO ESTABELECER CONEXÃO COM O BANCO DE DADOS (' . $connection->connect_errno . ' | ' . $connection->connect_error . ')');
        exit();
    }
 }catch(Exception $e){
    echo('[ERROR 03] - ERRO AO ESTABELECER CONEXÃO COM O BANCO DE DADOS (' . $e->getMessage() . ')');
    exit();
 }

 if(!$connection->select_db(DB_SCHEMA)){
    echo('[ERROR 04] - ERRO AO SELECIONAR O BANCO DE DADOS (' . DB_SCHEMA . ')');
    exit();
 }

 $connection->query("SET CHARACTER SET ". DB_CHARSET .";");
 ///  <<< CONEXÃO COM O BANCO DE DADOS

 
 ///  SELECT DOS DADOS >>>
 $sql = "
    SELECT
        bc.nome AS nome_do_banco,
        cv.verba,
        ct.codigo AS codigo_do_contrato,
        ct.data_inclusao,
        ct.valor,
        ct.prazo
    FROM
        tb_contrato ct
        INNER JOIN tb_convenio_servico cs ON ct.convenio_servico = cs.codigo
        INNER JOIN tb_convenio cv ON cs.convenio = cv.codigo
        INNER JOIN tb_banco bc ON cv.banco = bc.codigo
    ORDER BY
        ct.data_inclusao DESC;
 ";
 
 try{
    ///  TENTA EXECUTAR QUERY >>>
    if(!$rs = $connection->query($sql)){
        echo("[ERROR 05] - QUERY:\n" . $sql . "\n" . $connection->error . "\n");
        exit();
    }
    ///  <<< TENTA EXECUTAR QUERY
    ///  CASO QUERY TENHA DADO ERRO >>>
 }catch(Exception $ex){
    echo("[ERROR 06] - RECORDSET:\n" . $ex->errorMessage() . "\n");
    exit();
 }
 ///  <<< CASO QUERY TENHA DADO ERRO

 ///  RESULTADO DA CONSULTA >>>
 if(is_object($rs)){
    $result = $rs->fetch_all(MYSQLI_ASSOC);
 }else{
    $result = false;
 }
 ///  <<< RESULTADO DA CONSULTA
 ///  <<< SELECT DOS DADOS

 ///  LISTAGEM DOS DADOS >>>

 if($result === false){
    echo('CONSULTA SEM RETORNO!');
    exit();
 }

 if(count($result) > 0){
    $return = "";
    $div = "/t/t|/t/t";

    foreach($result as $row){

        $tmp = "";
        foreach($row as $key=>$value){
            $tmp .= $div . $key . " = " . $value;
        }

        $tmp = ltrim($tmp, $div);
        $return .= $tmp . "/n";
    }

    echo($return);
 }else{
    echo('NENHUM REGISTRO LOCALIZADO!');
    exit();
 }

 ///  <<< LISTAGEM DOS DADOS